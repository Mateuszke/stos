// #ifndef stoslista_hh

// #define stoslista_hh

#include <iostream>
#include <stdio.h>
#include <string.h>
#include "node.hh"
using namespace std;


template<typename typ>
class Stos{
    Node<typ> *top;             //wskazuje na górę stosu
    int rozmiar=0;             //zmienna przechowujaca ilosc elementow
    public:
    Stos(){
        top=NULL;                    
        }
    int cojakpusty(){                           //metoda wywoływana, gdy stos jest pusty i próbowano usunąć lub wyświetlić wszystkie lub 1 element        
        cout<<"Stos jest pusty"<<endl;
        return -1;
    }
    int size(){         // zwraca ilos elementow na stosie
        if(rozmiar==0){
            throw cojakpusty();
        }
        return rozmiar;
    }
    void push(typ n){                              //dodaje nowy węzeł jako na góre stostu
        Node<typ> *new_node = new Node<typ>;
        new_node->elem = n;                         //przypisanie danej
        new_node->next = top;                       //next nowego to "start" top
        top=new_node;                               //nowy wezel to top
        rozmiar=rozmiar+1;                          //jeden element wiecej
    }
    Node<typ> pop(){                            //usuwanie elementu ze stosu
        if(top==NULL){                          //co jesli juz pusty
            throw cojakpusty();
        }
        Node<typ> *old=top;                     //stary wezel jest topem
        typ x=old->elem;                        
        top=old->next;                          //nowy top to nastepny po starym topie
        rozmiar=rozmiar-1;                      //jeden element mniej
        return *old;
        delete old;                             //usun top
    }
    void popall(){
        while(top!=NULL){       //dopoki top nie bedzie NULL to usuwaj
            pop();
        }
        rozmiar=0;              //jak usunie wszystko to rozmiar=0
    }
    bool isEmpty(){             //sprawdza, czy pusty, jak top==NULL to tak
        if(top==NULL){  
            return true;
        }
        else{
            return false;
        }
    }
    const typ& whatsontop(){            //sprawdza co na górze stosu i zwraca daną
        if(top==NULL){                  //co jesli nic nie ma
            throw cojakpusty();
        }
        else{
            return top->elem;
        }
    }
    void display(){                 //wyswietla wszystkie dane kolejnych wezlow na stosie
        Node<typ>* tmp;
        tmp = top;                  //pomocniczy wezel jako top
        if(tmp==NULL){              //co jesli nic nie ma na stosie
            throw cojakpusty();
        }
        while (tmp != NULL) {           //dopoki top nie bedzie NULL, to wyswietlaj kolejną daną
            cout<< tmp->elem<<" ";
            tmp = tmp->next;            //przejdz do następnego
        }
   }
};
// #endif