// #ifndef node_hh
// #define node_hh

#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

template <typename typ>
class ListaJ;
template <typename typ>
class Stos;

template<typename typ>
class Node{
    private:
    typ elem;                   //przechowywana dana
    Node<typ>* next;            //wskaznik na kolejny wezel
    friend class ListaJ<typ>;
    friend class Stos<typ>;
    
    public:
    typ GetElem(){return elem;}
    Node getNext(){return next;}
    void setElement(typ newEl){
        elem=newEl;
    }
    void setNext(Node<typ> newN){
        next=newN;
    }
};
// #endif