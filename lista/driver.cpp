#include <iostream>
#include <stdio.h>
#include <string.h>
#include "stoslista.hh"
#include <time.h>
#include <iomanip>
#include <chrono>
using namespace std;

int main(){
    cout<<"Program przedstawia reprezentacje działań na strukturze STOS BAZUJĄCY NA LIŚCIE (jednokierunkowej), opiera swoje działanie na podklasie Node, która zawiera dane (o określonym typie) oraz wskaźnik do następnego węzła. Wykonując 'działania' na referencjach możemy dodawać oraz usuwać elementy z naszzego stosu."<<endl;
    Stos<int> stos;
    cout<<"UWAGA, prawie ctrl+c ctrl+v z drivera do stosu opartego na tablicy"<<endl;
    cout<<"Zdefiniowano nowy, pusty stos."<<endl;
    cout<<"Wywołanie metody 'isEmpty(), która zwraca wartość 1, gdy stos jest pusty i 0, gdy nie jest."<<endl;
    cout<<"stos.isEmpty(); :  "<<stos.isEmpty()<<endl;
    cout<<"Wywołanie metody 'push(arg)', powoduje dodanie argumentu metody (o określonym typie) na górę stosu: "<<endl;
    cout<<"Tworzymy nowy węzeł, przypisujemy mu podaną wartość, wskaźnnikiem pokazujemy na aktualny top, a następnie do 'top' stosu przypisujemy nowy węzeł"<<endl;
    cout<<"stos.push(1); stos.push(2);"<<endl;
    stos.push(1);stos.push(2);
    cout<<"stos.isEmpty(); :  "<<stos.isEmpty()<<endl;
    cout<<"Metoda 'whatsontop() zwraca element na górze stosu: "<<endl;
    cout<<"stos.whatsontop();  "<<stos.whatsontop()<<endl;
    cout<<"Metoda 'display()' pozwala na wyświetlenie całej zawartości stosu w kolejności od najnowszego (wyższego) elementu do najstarszego (najniższego)."<<endl;
    cout<<"stos.display():  ";stos.display();cout<<endl;
    cout<<"Metoda 'size()' zwraca aktualny rozmiar stosu:"<<endl;
    cout<<"stos.size();  :   "<<stos.size()<<endl;
    cout<<"Metoda 'pop()' usuwa i zwraca węzeł (tylko) na górze stosu"<<endl;
    cout<<"Tworzymy nowy węzeł, któremu przypisujemy węzeł na górze stosu, pod top stosu przypisujemy następny węzeł po tym, który usuwamy, usuwamy stary węzeł"<<endl;  
    cout<<"stos.pop();"<<endl;stos.pop();
    cout<<"stos.whatsontop();  :  "<<stos.whatsontop()<<endl;
    cout<<"Metoda 'popall()' pozwala na całkowite wyczyszczenie stosu"<<endl;
    cout<<"stos.push(1); stos.push(2);"<<endl;
    stos.push(1);stos.push(2);
    cout<<"stos.display();   :  ";stos.display();cout<<endl;
    cout<<"Gdy stos jest pusty, to metoda display, pop, oraz whatsontop powodują wywołanie metody cojakpusty(), która informuje o tym, że stos jest pusty."<<endl;
    cout<<"Tak samo dla funkcji push(), gdy tablica reprezentująca stos jest zapełniona, wywołuje się metoda co jak pełny, która informuje o zapełnieniu tablicy."<<endl;
    cout<<"stos.popall();"<<endl;stos.popall();
    cout<<"stos.display();  :  ";stos.display();
    cout<<endl<<endl;
    
    cout<<"Klasa Node, zawiera daną oraz wskaźnik na następny węzeł. Posiada metody pozwalające na ustawienie oraz pobranie danej, oraz ustawienie i pobranie następnego węzła"<<endl;
    Node<int> a,b;
    a.setElement(10);
    a.setNext(b);
    cout<<"Node<int> a,b; \n a.setElement(10); \n a.setNext(b);";
}