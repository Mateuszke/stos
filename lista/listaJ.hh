#ifndef listaJ.hh
#define listaJ.hh

#include <iostream>
#include <stdio.h>
#include <string.h>
#include "node.hh"
using namespace std;

template<typename typ>
class ListaJ{
    Node<typ> *head;
    Node<typ> *tail;
    public:
    ListaJ(){
        head=NULL;tail=NULL;
        }
    void addFront(typ n){
        Node<typ> *new_node = new Node<typ>;
        new_node->elem = n;
        new_node->next = head;
        head=new_node;
    }
    void removeFront(){
        Node<typ> *old=head;
        head=old->next;
        delete old;
    }
    bool isEmpty(){
        if(head=NULL){
            return true;
        }
        else{return false;}
    }
    const typ& front(){
        return head->elem;
    }
};
#endif