#include <iostream>
#include <stdio.h>
#include <string.h>
#include "stoslista.hh"
#include <time.h>
#include <iomanip>
#include <chrono>
using namespace std;



int main(){
    int max=100000; //ilosc elementow do dodania
    Stos<int> a;    
    auto start = chrono::high_resolution_clock::now();  //start odliczania
    for(int i=0;i<max;i++){
        int r = (rand() % 100) + 1;       //losowa od 1-100
        a.push(r);
    }
    auto stop = chrono::high_resolution_clock::now(); //stop odliczania
    double duration = chrono::duration_cast<chrono::nanoseconds>(stop - start).count();
    duration*=1e-9;
    cout<<"Czas trwania: "<<fixed<<duration<<setprecision(9)<<" sekund"<<endl;
    return 0;
    

};