#include <iostream>
#include <stdio.h>
#include <string.h>
#include <chrono>
#include <iomanip>
using namespace std;
#define MAX 100000
#include "stostab.hh"

int main(){
    
    cout<<"Program przedstawia reprezentacje działań na strukturze STOS BAZUJĄCY NA TABLICY, operując indeksem tablicy, który wskazuje element na górze stosu, rozmiar tablicy trzeba wcześniej określić."<<endl;
    TabStos<int> stos;
    cout<<"Zdefiniowano nowy, pusty stos."<<endl;
    cout<<"Wywołanie metody 'isEmpty(), która zwraca wartość 1, gdy stos jest pusty i 0, gdy nie jest."<<endl;
    cout<<"stos.isEmpty(); :  "<<stos.isEmpty()<<endl;
    cout<<"Wywołanie metody 'push(arg)', powoduje dodanie argumentu metody (o określonym typie) na górę stosu: "<<endl;;
    cout<<"stos.push(1); stos.push(2);"<<endl;
    stos.push(1);stos.push(2);
    cout<<"stos.isEmpty(); :  "<<stos.isEmpty()<<endl;
    cout<<"Metoda 'whatsontop() zwraca element na górze stosu: "<<endl;
    cout<<"stos.whatsontop();  "<<stos.whatsontop()<<endl;
    cout<<"Metoda 'display()' pozwala na wyświetlenie całej zawartości stosu w kolejności od najnowszego (wyższego) elementu do najstarszego (najniższego)."<<endl;
    cout<<"stos.display():  ";stos.display();
    cout<<"Metoda 'size()' zwraca aktualny rozmiar stosu:"<<endl;
    cout<<"stos.size();  :   "<<stos.size()<<endl;
    cout<<"Metoda 'pop()' usuwa i zwraca element (tylko) na górze stosu"<<endl;
    cout<<"stos.pop();"<<endl;stos.pop();
    cout<<"stos.whatsontop();  :  "<<stos.whatsontop()<<endl;
    cout<<"Metoda 'popall()' pozwala na całkowite wyczyszczenie stosu"<<endl;
    cout<<"stos.push(1); stos.push(2);"<<endl;
    stos.push(1);stos.push(2);
    cout<<"stos.display();   :  ";stos.display();
    cout<<"Gdy stos jest pusty, to metoda display, pop, oraz whatsontop powodują wywołanie metody cojakpusty(), która informuje o tym, że stos jest pusty."<<endl;
    cout<<"Tak samo dla funkcji push(), gdy tablica reprezentująca stos jest zapełniona, wywołuje się metoda co jak pełny, która informuje o zapełnieniu tablicy."<<endl;
    cout<<"stos.popall();"<<endl;stos.popall();
    cout<<"stos.display();  :  ";stos.display();




};