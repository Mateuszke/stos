#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;

template<typename typ>
class TabStos{
        int top; //indeks tablicy
        int pojemnosc;          //maksymalna pojemnosc tablicy, jej rozmiar
        typ *tab; 
    public:
        TabStos(){        //konstruktor
            top=-1;
            pojemnosc=MAX;
            tab = new typ[pojemnosc];
        }
        int cojakpelny(){               //metoda wywoływana, gdy tablica (stos) jest zapełniona i próbowano dodać kolejny element
            cout<<"Tablica została przepełniona"<<endl;
            return 1;
        }
        int cojakpusty(){               //metoda wywoływana, gdy tablica (stos) jest pusta i próbowano usunąć lub wyświetlić wszystkie lub 1 element
            cout<<"Stos jest pusty"<<endl;
            return 2;
        }
        int size(){                //zwraca ilość elementów stosu
            return top+1;
        }
        bool isEmpty(){            //zwraca czy stos jest pusty
            return (top<0);
        }
        typ& whatsontop(){              //zwraca 1 element (na górze) stosu
            if(top<0){
                throw cojakpusty();
            }
            else{
                return tab[top];
            }
        }
        void push(typ cos){             //dodaje element na górę stosu
            if(top>=(MAX-1)){
                throw cojakpelny();
            }
            top=top+1;        //przesuniecie indeksu wskazujacego na górę stosu o 1 do przodu
            tab[top]=cos;
            
        }
        typ pop(){                      //usuwa element z góry stosu
            typ x;
            if(top<0){
                throw cojakpusty();
            }
            x=tab[top];
            top=top-1;              //przesuniecie indeksu wskazujacego na górę stosu o 1 do tyłu
            return x;
        }
        void display(){                     //wyświetla wszystkie elementy
            if(top<0){
                throw cojakpusty();
            }
            for(int i=top;i>=0;i--){     //wyświetlanie kolejno elementów (od góry do dołu)
                cout<<tab[i]<<" ";
            }
            cout<<endl;
        }
        void popall(){      //usuwa wszystkie elementy stosu
            if (top==-1){
                throw cojakpusty();
            }
            top=-1;          //ustawienie top na domyślne -1, aby "zacząc od nowa"
        }

};