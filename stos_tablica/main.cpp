#include <iostream>
#include <stdio.h>
#include <string.h>
#include <chrono>
#include <iomanip>
using namespace std;
#define MAX 100000
#include "stostab.hh"



int main(){
    TabStos<int> a;
    auto start = chrono::high_resolution_clock::now();                                          //start odliczania czasu
    for(int i=0;i<MAX;i++){
        int r = (rand() % 100) + 1;                                                     //losowa liczba z zakresu 1-100
        a.push(r);
    }
    auto stop = chrono::high_resolution_clock::now();                                   //stop odlicania czasu
    double duration = chrono::duration_cast<chrono::nanoseconds>(stop - start).count();
    duration*=1e-9;
    cout<<"Czas trwania: "<<fixed<<duration<<setprecision(9)<<" sekund"<<endl;
    //a.display();
    return 0;
};

    


